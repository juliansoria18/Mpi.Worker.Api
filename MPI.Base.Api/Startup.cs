﻿using System;
using System.Configuration;
using System.Web.Http;
using IdentityServer3.AccessTokenValidation;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using MPI.Base.Api.App_Start;
using MPI.Base.Api.Authorization;
using MPI.Base.Api.Models;
using Owin;

[assembly: OwinStartup(typeof(MPI.Base.Api.Startup))]

namespace MPI.Base.Api
{
    public class Startup
    {
        //public void Configuration(IAppBuilder app)
        //{
        //    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888

        //    app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
        //    var myProvider = new MyOAuthAuthorizationServerProvider();
        //    OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions
        //    {
        //        AllowInsecureHttp = true,
        //        TokenEndpointPath = new PathString("/api/auth/token"),
        //        AccessTokenExpireTimeSpan = TimeSpan.FromDays(365),
        //        Provider = myProvider
        //    };
        //    app.UseOAuthAuthorizationServer(options);
        //    app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        //    HttpConfiguration config = new HttpConfiguration();
        //    //config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        //    WebApiConfig.Register(config);

        //    AutoMapperConfiguration.Initialize();
        //}

        public void Configuration(IAppBuilder app)
        {
            // Para obtener más información acerca de cómo configurar su aplicación, visite http://go.microsoft.com/fwlink/?LinkID=316888
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
            {
                Authority = ConfigurationManager.AppSettings["AuthServer"],
                ValidationMode = ValidationMode.ValidationEndpoint,
                RequiredScopes = new[] { "authserver" }

            });
            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
            app.UseWebApi(config);

            AutoMapperConfiguration.Initialize();
        }
    }
}
