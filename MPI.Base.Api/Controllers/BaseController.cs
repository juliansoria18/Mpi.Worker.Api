﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using MPI.Base.Api.Models;

namespace MPI.Base.Api.Controllers
{
    [Authorize]
    public class BaseController<T> : ApiController where T : Models.Base, new()
    {
        public readonly IRepository<T> Repository;

        protected BaseController(
            IRepository<T> repository)
        {
            Repository = repository;
        }

        [HttpGet, Route("all")]
        public IEnumerable<T> GetAll()
        {
            return Repository.GetAll();
        }

        [HttpGet, Route("async/all")]
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Repository.GetAllAsync();
        }

        [HttpGet, Route("id/{id}")]
        public T GetById(string id)
        {
            return Repository.GetById(id);
        }

        [HttpGet, Route("async/id/{id}")]
        public async Task<T> GetByIdAsync(string id)
        {
            return await Repository.GetByIdAsync(id);
        }

        [HttpGet, Route("count")]
        public int Count()
        {
            return Repository.Count(null);
        }

        [HttpGet, Route("async/count")]
        public async Task<int> CountAsync()
        {
            return await Repository.CountAsync(null);
        }

        [HttpPost, Route("add")]
        public void InsertAndSave([FromBody]T value)
        {
            Repository.InsertAndSave(value);
        }

        [HttpPost, Route("async/add")]
        public async Task InsertAndSaveAsync([FromBody]T value)
        {
            await Repository.InsertAndSaveAsync(value);
        }

        [HttpPut, Route("edit/{id}")]
        public void UpdateAndSave(string id, [FromBody]T value)
        {
            Repository.UpdateAndSave(value);
        }

        [HttpPut, Route("async/edit/{id}")]
        public async Task UpdateAndSaveAsync(string id, [FromBody]T value)
        {
            await Repository.UpdateAndSaveAsync(value);
        }

        [HttpDelete, Route("delete/{id}")]
        public void DeleteByIdAndSave(string id)
        {
            Repository.DeleteByIdAndSave(id);
        }

        [HttpDelete, Route("async/delete/{id}")]
        public async Task DeleteByIdAndSaveAsync(string id)
        {
            await Repository.DeleteByIdAndSaveAsync(id);
        }
    }
}
