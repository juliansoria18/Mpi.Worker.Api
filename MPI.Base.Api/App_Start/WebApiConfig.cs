﻿using System.Web.Http;
using MPI.Base.Api.App_Start;
using MPI.Base.Api.Models;
using MPI.Base.Api.Resolvers;
using Unity;
using Unity.Lifetime;

namespace MPI.Base.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web

            // Rutas de API web
            config.MapHttpAttributeRoutes(new InheritanceDirectRouteProvider());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var container = new UnityContainer();

            container.RegisterType(typeof(IRepository<>), typeof(Repository<>), new HierarchicalLifetimeManager());
            //container.RegisterType<IComputerTypeRepository, ComputerTypeRepository>(new HierarchicalLifetimeManager());

            config.DependencyResolver = new UnityResolver(container);

            config.Filters.Add(new ValidateModelAttribute());
        }
    }
}
