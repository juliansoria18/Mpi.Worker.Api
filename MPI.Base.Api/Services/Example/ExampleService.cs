﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json;

namespace MPI.Base.Api.Services.Example
{
    public class ExampleService
    {
        private static ExampleService _instance;
        private readonly Database _fusionDatabase;

        public ExampleService()
        {
            var databaseProviderFactory = new DatabaseProviderFactory();
            _fusionDatabase = databaseProviderFactory.Create("YOUR_DB_CONNECTION_STRING_NAME_IN_WEBCONFIG");
        }

        public static ExampleService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ExampleService();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Este método devuelve una colección de objetos obtenidos a partir de la ejecución de un store procedure.
        /// Puede reemplazar el tipo de dato genérico "dynamic" por su propio DTO.
        /// </summary>
        /// <param name="yourParam1"></param>
        /// <param name="yourParam2"></param>
        /// <param name="yourParam3"></param>
        /// <returns></returns>
        public ICollection<dynamic> ExampleCollectionObjects(string yourParam1, string yourParam2, string yourParam3)
        {
            try
            {
                var stSqlCommand = "[YOUR_DB_NAME].[YOUR_SCHEMA].[YOUR_STORE_PROCEDURE]";
                var dbCommand = _fusionDatabase.GetStoredProcCommand(stSqlCommand);

                _fusionDatabase.AddInParameter(dbCommand, "@YourParam1", DbType.String, yourParam1);
                _fusionDatabase.AddInParameter(dbCommand, "@YourParam2", DbType.String, yourParam2);
                _fusionDatabase.AddInParameter(dbCommand, "@YourParam3", DbType.String, yourParam3);

                var result = _fusionDatabase.ExecuteDataSet(dbCommand).Tables[0];

                if (result == null || result.Rows.Count <= 0) return new List<dynamic>();
                var serialized = JsonConvert.SerializeObject(result);
                var deserialized = JsonConvert.DeserializeObject<List<dynamic>>(serialized);
                return deserialized;
            }
            catch (Exception)
            {
                return new List<dynamic>();
            }
        }

        /// <summary>
        /// Este método devuelve un objeto obtenido a partir de la ejecución de un store procedure
        /// Puede reemplazar el tipo de dato genérico "dynamic" por su propio DTO.
        /// </summary>
        /// <param name="yourParam1"></param>
        /// <param name="yourParam2"></param>
        /// <param name="yourParam3"></param>
        /// <returns></returns>
        public dynamic ExampleSingleObjects(string yourParam1, string yourParam2, string yourParam3)
        {
            try
            {
                var stSqlCommand = "[YOUR_DB_NAME].[YOUR_SCHEMA].[YOUR_STORE_PROCEDURE]";
                var dbCommand = _fusionDatabase.GetStoredProcCommand(stSqlCommand);

                _fusionDatabase.AddInParameter(dbCommand, "@YourParam1", DbType.String, yourParam1);
                _fusionDatabase.AddInParameter(dbCommand, "@YourParam2", DbType.String, yourParam2);
                _fusionDatabase.AddInParameter(dbCommand, "@YourParam3", DbType.String, yourParam3);

                var result = _fusionDatabase.ExecuteDataSet(dbCommand).Tables[0];

                if (result == null || result.Rows.Count <= 0) return null;
                var serialized = JsonConvert.SerializeObject(result);
                var deserialized = JsonConvert.DeserializeObject<List<dynamic>>(serialized);
                return deserialized[0];
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}