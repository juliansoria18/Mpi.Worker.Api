﻿IF schema_id('logistics') IS NULL
    EXECUTE('CREATE SCHEMA [logistics]')
CREATE TABLE [logistics].[SectorTypes] (
    [Id] [nvarchar](64) NOT NULL,
    [Name] [nvarchar](128) NOT NULL,
    [Description] [nvarchar](1024),
    [RegisterDate] [datetime],
    [RegisterBy] [nvarchar](128),
    [UpdatedDate] [datetime],
    [UpdatedBy] [nvarchar](128),
    [DeletedDate] [datetime],
    [DeletedBy] [nvarchar](128),
    [IsDeleted] [bit] NOT NULL,
    CONSTRAINT [PK_logistics.SectorTypes] PRIMARY KEY ([Id])
)
CREATE TABLE [logistics].[Sectors] (
    [Id] [nvarchar](64) NOT NULL,
    [Name] [nvarchar](128) NOT NULL,
    [Description] [nvarchar](1024),
    [SectorTypeId] [nvarchar](64),
    [ParentId] [nvarchar](64),
    [FullId] [nvarchar](max),
    [RegisterDate] [datetime],
    [RegisterBy] [nvarchar](128),
    [UpdatedDate] [datetime],
    [UpdatedBy] [nvarchar](128),
    [DeletedDate] [datetime],
    [DeletedBy] [nvarchar](128),
    [IsDeleted] [bit] NOT NULL,
    CONSTRAINT [PK_logistics.Sectors] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_SectorTypeId] ON [logistics].[Sectors]([SectorTypeId])
CREATE INDEX [IX_ParentId] ON [logistics].[Sectors]([ParentId])
ALTER TABLE [logistics].[Sectors] ADD CONSTRAINT [FK_logistics.Sectors_logistics.Sectors_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [logistics].[Sectors] ([Id])
ALTER TABLE [logistics].[Sectors] ADD CONSTRAINT [FK_logistics.Sectors_logistics.SectorTypes_SectorTypeId] FOREIGN KEY ([SectorTypeId]) REFERENCES [logistics].[SectorTypes] ([Id])
INSERT [dbo].[__MigrationHistory]([MigrationId], [ContextKey], [Model], [ProductVersion])
VALUES (N'201906240301222_Add_Sector_And_SectorType_Model', N'MPI.Base.Api.Migrations.Configuration',  0x1F8B0800000000000400ED5BDB6EE336107D2FD07F10F4D41659CBC916C536B07791D849117473419C2CFA1630D2D82196A25491CADA28FA657DE827F5173ABA5357CBB26364B14280C416396786A3D1195233F9EF9F7F471F9636D39EC113D4E163FD7030D435E0A66351BE18EBBE9CBF79A77F78FFFD77A333CB5E6A9F92796F837928C9C5587F92D23D360C613E814DC4C0A6A6E708672E07A6631BC4728CA3E1F057E3F0D00084D0114BD346B73E97D486F00B7E9D38DC0457FA845D3A1630115FC7915988AA5D111B844B4C18EB9737178353226070E2D241345DD74E182568CA0CD85CD708E78E24120D3DBE1730939EC31733172F1076B77201E7CD0913102FE0389BDE762DC3A3602D4626984099BE908EBD21E0E1DBD8394651BC938BF5D479E8BE3374B35C05AB0E5D38D6278EEDFA123C5D2B2A3B9E302F9858E9E14122977D3AD0D47907696C6008053F07DAC467D2F760CCC1971E6107DA8DFFC8A8F93BACEE9CCFC0C7DC674C3516CDC5B1DC05BC74E3392E7872750BF378091796AE197939A328988A2932D1E2301C30B475ED922C3F025FC8A7B1FECBCFBA764E97602517E2F0B8E7141F0494919E8F5FAFD05EF2C8201D371A5506BF1B941E1EBD7B09AD5310A647DD287AEA950F8FDAADB959D92D2CA8C0509812992E35F87C47EDD6B2A7AB1D38A959D5BD6BA15156172B63D13D183905061D8D8C45F760E4858875259A4E1D8701E16BC27464643CD4C84E3330A513B2D0A6FC9449AA9F7B8EEA39AAE7A89EA35A7014025D9167BA08B9A60019110A6E336F8185E3E289BAD16E33269B0795B9CE3DC7BE75582AA80C3EDC116F01124D73EA66CC1CDF33B723D06EE499FCED49F39B27CD2C1C77E1DF665D37C4032E5F5ECF39FABB510B7EECF34D9F6F5E41BE891E89A67493CC28A59A68204D228534138F264968B3FC1725B7ED53608D6D1549B24D0A3C11C231696852CEDAC445F9259E714B6BF457142BD9AA306030DB5117F31B2A1FEB3F95BC56079966FA7590C3C1E0B0848A591102184AD8049D8D799672594EA1949BD425ACC9808250CBCC1B383C852F8E4CC1056EA18E265FB6D19B717F597BAAA4B02358E79991A18444AB48510277CDADAD8AE21D444CD5FEB0081B3D572F1C396543F6173D65DFB6D19DDFA9EC218A221642198912E0C5669CB8C14D09274F1F834158CA8A5DF8BD8078232EE2E4528C8C007C06B2F0B6168F1E19FBC56191BDC92D85571E4479355201A346572BA07A901280E2C2224A9AC194399529AE7847D731786AB4626F292CD671F67A905C7CE0C4F60B571D5EBBF83A4A6A494ADD9C50414325A0388C5A3B2349DBE9039395768CA8B693D4808C9A22D0E892B82E6EDD94A2507C459B4515A1C99BD9E695123BC2304C51513049AD4D35E1D2C9020AA3C1036AC139F584C47D297924C1266F62D9A56995F450F3B0252A8B0C50BE7DC9439848049F23A9A60A4E05ADC600E7B8443BE0E560B5A0DCFA7AD1B0424718F12A8EE51387F936AF4F11F5D2D1095B958FAEB447C89D9655A0DC407BBCFCA94E05CC8F6C8E189C50AAF082EBEDD172C739152E37B0315ED138E5F226F74239C5E5EF8532B0315ED136E5727B2CE5F0960BD7EC72196B64149E97D2A6A3F46816D8B2F8ACB76202957FB7E582FAF4D2820D9A847B3EE8F960FDBDE8F960677CB02B2EE8CC03DF2E07E48F9E2A60F3A1B41E317B1DA2A2D5BF24A9474A5E76AB38C9B59EE57A967B652C573A2516A7A4DAD3D362E154388A4F68EBFB074B47B6688AAEA1939EA9151CD7662B8C457B104C18CCFE641346C35715C9844BC2E91C848CAA9DFAD1F0F0A8D081F87ABA010D212CD6B92570BAE204E1CF29C3F18764E2431A28633D42C398B0E18BE37D1EE42444FEEB14E694D370297BAF0EF367E2994FC42BD515B728FE56628645A5ED6BBBD5D861753703EF5A8F0CE84C8645B46E585955AD8D0B3A9623BB1A592A4FEECAC68A6A64571B4BD5C95DD9582A463E52B997E6BC3C53286F127BAEE8B9A2E78A6F932B5AF244CF115F114754B58665E0A5FB7DC12D588EF5BF42E963EDE28F0715E040BBF670437DAC0DB5BF1B3CD9A58D6C239B12E11DDA936F374BACF9C126CB1F7B5AEE69B981637647CBDBF546756D5D2AE24447E20E7D325B74B2ECAF6F255C5D1B6D5F59AF53B4CFEFDE86D4219A761A0135C5AD178A82AFA957A9DCB350536F28BD1F6AE8418A5EA28D75CA9FD152C70B6831E22BA587A975A3D29A3EA54419738264464D912A6B6A52A956D7D0CDB44E4D59C53E1A9E2A9A725A34F74424BC56F2F5B435A961502C3BAF5FEE764BDDA069A9FCDA199F57E59FDB912F045D6410C1BFBA73B4477D52D339177CEE24B451B0289952D82C5C8224B88321279EA473624A1C364188B0D3FD13613E4E39B31FC1BAE0D7BEC4C710970CF623CB550B02E269D21F7666E56D1E5D87870BB18B25A09934D8845DF3539F322BB5FBBC629F53031130DA6F80D7A37B89042961B14A91AE1CDE1228765F4AC47760BB0CC1C4359F9167E862DBBD808FB020E62AA91ED483ACBF1179B78FA6942CF0A42C628C4C1EBF620C5BF6F2FDFF39AB3448E3410000 , N'6.2.0-61023')