﻿namespace MPI.Base.Api.Authorization
{
    public class IdentityServerRole
    {
        public string CodSistema { get; set; }
        public string CodTipoUsuario { get; set; }
        public string TipoUsuario { get; set; }
    }
}