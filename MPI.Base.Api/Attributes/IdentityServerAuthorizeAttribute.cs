﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using MPI.Base.Api.Authorization;
using Newtonsoft.Json;

namespace MPI.Base.Api.Attributes
{
    public class IdentityServerAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext httpActionContext)
        {
            base.OnAuthorization(httpActionContext);

            var sub = ClaimsPrincipal.Current.FindAll("sub").Select(c => c.Value).FirstOrDefault();
            if (!string.IsNullOrEmpty(sub))
            {
                try
                {
                    var identityServerUser = JsonConvert.DeserializeObject<IdentityServerUser>(sub);
                    if (identityServerUser == null || !identityServerUser.Roles.Any(s => s.CodSistema.Equals(ConfigurationManager.AppSettings["ApplicationCode"])))
                    {
                        HttpContext.Current.Response.StatusCode = 403;
                        httpActionContext.Response = new HttpResponseMessage
                        {
                            StatusCode = HttpStatusCode.Forbidden,
                            Content = new StringContent("No posee autorización para acceder a esta aplicación.")
                        };
                    }
                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.StatusCode = 500;
                    httpActionContext.Response = new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.InternalServerError,
                        Content = new StringContent("Ocurrio un error en el atributo de autorización [ServerAuthorize].")
                    };
                }
            }
        }
    }
}