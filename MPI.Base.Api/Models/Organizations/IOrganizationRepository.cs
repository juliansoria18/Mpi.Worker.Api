﻿namespace MPI.Base.Api.Models.Organizations
{
    public interface IOrganizationRepository : IRepository<Organization>
    {
    }
}
