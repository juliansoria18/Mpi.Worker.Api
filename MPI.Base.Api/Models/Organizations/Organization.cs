﻿using MPI.Base.Api.Models.EntityTypes;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Base.Api.Models.Organizations
{
    [Table("catalog.Organizations")]
    public class Organization : Base
    {
        public Organization()
        {
            EntityTypes = new HashSet<EntityType>();
            //EntityRelationships = new HashSet<EntityRelationship>();
            //EntitySurveyRelationships = new HashSet<EntitySurveyRelationship>();
            //OrganizationsBranches = new HashSet<OrganizationBranch>();
        }

        [Column("Organization")]
        [StringLength(100)]
        public string OrganizationName { get; set; }

        public string Color { get; set; }

        public string Icon { get; set; }


        [JsonIgnore]
        public virtual ICollection<EntityType> EntityTypes { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<EntityRelationship> EntityRelationships { get; set; }

        //[JsonIgnore]
        //public virtual ICollection<EntitySurveyRelationship> EntitySurveyRelationships { get; set; }
    }
}