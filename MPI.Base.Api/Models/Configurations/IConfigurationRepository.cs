﻿namespace MPI.Base.Api.Models.Configurations
{
    public interface IConfigurationRepository : IRepository<Configuration>
    {
    }
}
