﻿using MPI.Base.Api.Models.Items;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Base.Api.Models.Configurations
{
    [Table("config.Configuration")]
    public class Configuration : Base
    {
        public Configuration()
        {
            Items = new HashSet<Item>();
        }

        public int Active { get; set; }

        public bool Rechargeable { get; set; }

        public int Duration { get; set; }

        public bool History { get; set; }

        public bool Reading { get; set; }

        public bool SearchKey { get; set; }

        public bool Restrictive { get; set; }

        public string CodDependencyBlock { get; set; }

        public bool MassiveEdition { get; set; }

        public string CodAction { get; set; }

        public int Ordering { get; set; }

        public string Identificacion { get; set; }

        public string DefaultValue { get; set; }

        public string Help { get; set; }

        public bool Mandatory { get; set; }

        public bool Visible { get; set; }

        public string Mask { get; set; }

        public bool ShowOrderOption { get; set; }

        [JsonIgnore]
        public virtual ICollection<Item> Items { get; set; }
    }
}