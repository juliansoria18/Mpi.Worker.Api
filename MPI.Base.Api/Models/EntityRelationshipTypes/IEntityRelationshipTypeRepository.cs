﻿namespace MPI.Base.Api.Models.EntityRelationshipTypes
{
    public interface IEntityRelationshipTypeRepository : IRepository<EntityRelationshipType>
    {
    }
}
