﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using MPI.Base.Api.Models.EntityRelationships;
using Newtonsoft.Json;

namespace MPI.Base.Api.Models.EntityRelationshipTypes
{

    [Table("catalog.EntityRelationshipTypes")]
    public class EntityRelationshipType : Base
    {
        public EntityRelationshipType()
        {
            EntityRelationships = new HashSet<EntityRelationship>();
        }

        [Column("EntityRelationshipType")]
        public string EntityRelationshipType1 { get; set; }

        [JsonIgnore]
        public virtual ICollection<EntityRelationship> EntityRelationships { get; set; }
    }
}