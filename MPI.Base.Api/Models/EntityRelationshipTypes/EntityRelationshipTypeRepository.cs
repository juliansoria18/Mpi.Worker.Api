﻿namespace MPI.Base.Api.Models.EntityRelationshipTypes
{
    public class EntityRelationshipTypeRepository : Repository<EntityRelationshipType>, IEntityRelationshipTypeRepository
    {
        public EntityRelationshipTypeRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}