﻿using AutoMapper;

namespace MPI.Base.Api.Models
{
    public static class AutoMapperConfiguration
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
            });
        }
    }
}