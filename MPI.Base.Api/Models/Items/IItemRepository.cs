﻿namespace MPI.Base.Api.Models.Items
{
    public interface IItemRepository : IRepository<Item>
    {
    }
}
