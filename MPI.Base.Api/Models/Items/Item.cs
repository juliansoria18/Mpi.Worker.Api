﻿using MPI.Base.Api.Models.Configurations;
using MPI.Base.Api.Models.EntityTypes;
using MPI.Base.Api.Models.ItemTypes;
using MPI.Base.Api.Models.Versions;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Base.Api.Models.Items
{
    [Table("config.Item")]
    public class Item : Base
    {
        public Item()
        {
        }

        public string VersionId { get; set; }

        public Version  Version { get; set; }

        [Required]
        [StringLength(256)]
        [Column("Item")]
        public string ItemName { get; set; }

        public string EntityTypeId { get; set; }

        public virtual EntityType EntityType { get; set; }

        public string DataTypeId { get; set; }

        public virtual DataType DataType { get; set; }

        public string ItemParent { get; set; }

        public int Ordering { get; set; }

        public string ItemTypeId { get; set; }

        public virtual ItemType ItemType { get; set; }

        public string ConfigurationId { get; set; }

        [JsonIgnore]
        public virtual Configuration Configuration { get; set; }
    }
}