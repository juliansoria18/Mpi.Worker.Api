﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Core.Metadata.Edm;
using Newtonsoft.Json;
using DataType = MPI.Base.Api.Models.DataTypes.DataType;

namespace MPI.Base.Api.Models.EntityFields
{

    [Table("config.EntityFields")]
    public class EntityField : Base
    {
        public EntityField()
        {
        }

        [Column("EntityField")]
        public string EntityFieldName { get; set; }

        public int Ordering { get; set; }

        [Required]
        public string EntityTypeId { get; set; }

        [JsonIgnore]
        public virtual EntityType EntityType { get; set; }

        [Required]
        [StringLength(100)]
        public string DataTypeId { get; set; }

        [JsonIgnore]
        public virtual DataType DataType { get; set; }
    }
}