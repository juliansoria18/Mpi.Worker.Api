﻿namespace MPI.Base.Api.Models.EntityFields
{
    public interface IEntityFieldRepository : IRepository<EntityField>
    {
    }
}
