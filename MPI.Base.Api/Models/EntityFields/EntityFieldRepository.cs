﻿namespace MPI.Base.Api.Models.EntityFields
{
    public class EntityFieldRepository : Repository<EntityField>, IEntityFieldRepository
    {
        public EntityFieldRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}