﻿using MPI.Base.Api.Models.Items;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Base.Api.Models.ItemTypes
{
    [Table("catalog.ItemTypes")]
    public class ItemType : Base
    {
        public ItemType()
        {
            Items = new HashSet<Item>();
        }
        
        [StringLength(100)]
        public string Name { get; set; }

        public string Description { get; set; }

        [JsonIgnore]
        public virtual ICollection<Item> Items { get; set; }
    }
}