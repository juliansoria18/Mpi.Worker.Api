﻿namespace MPI.Base.Api.Models.ItemTypes
{
    public class ItemTypeRepository : Repository<ItemType>, IItemTypeRepository
    {
        public ItemTypeRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}