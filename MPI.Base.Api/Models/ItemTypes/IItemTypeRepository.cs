﻿namespace MPI.Base.Api.Models.ItemTypes
{
    public interface IItemTypeRepository : IRepository<ItemType>
    {
    }
}
