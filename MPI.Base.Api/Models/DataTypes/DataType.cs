﻿using MPI.Base.Api.Models.EntityFields;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Base.Api.Models.DataTypes
{
    [Table("catalog.DataTypes")]
    public class DataType : Base
    {
        public DataType()
        {
            EntityFields = new HashSet<EntityField>();
        }
        
        [StringLength(100)]
        public string Name { get; set; }

        public string Description { get; set; }

        [JsonIgnore]
        public virtual ICollection<EntityField> EntityFields { get; set; }
    }
}