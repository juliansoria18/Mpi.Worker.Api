﻿namespace MPI.Base.Api.Models.DataTypes
{
    public class DataTypeRepository : Repository<DataType>, IDataTypeRepository
    {
        public DataTypeRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}