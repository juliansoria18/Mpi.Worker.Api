﻿namespace MPI.Base.Api.Models.DataTypes
{
    public interface IDataTypeRepository : IRepository<DataType>
    {
    }
}
