﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MPI.Base.Api.Models
{
    public class Base
    {
        [StringLength(64)]
        public string Id { get; set; } = Guid.NewGuid().ToString();

        public DateTime? RegisterDate { get; set; }

        [StringLength(128)]
        public string RegisterBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        [StringLength(128)]
        public string UpdatedBy { get; set; }

        public DateTime? DeletedDate { get; set; }

        [StringLength(128)]
        public string DeletedBy { get; set; }

        public bool IsDeleted { get; set; } = false;
    }
}
