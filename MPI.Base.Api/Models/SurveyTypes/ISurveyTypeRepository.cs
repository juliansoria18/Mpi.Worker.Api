﻿namespace MPI.Base.Api.Models.SurveyTypes
{
    public interface ISurveyTypeRepository : IRepository<SurveyType>
    {
    }
}
