﻿using MPI.Base.Api.Models.EntityFields;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Base.Api.Models.SurveyTypes
{
    [Table("catalog.SurveyTypes")]
    public class SurveyType : Base
    {
        public SurveyType()
        {
            EntityFields = new HashSet<EntityField>();
        }
        
        [StringLength(100)]
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual ICollection<EntityField> EntityFields { get; set; }

    }
}