﻿namespace MPI.Base.Api.Models.SurveyTypes
{
    public class SurveyTypeRepository : Repository<SurveyType>, ISurveyTypeRepository
    {
        public SurveyTypeRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}