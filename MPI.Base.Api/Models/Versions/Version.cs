﻿using MPI.Base.Api.Models.SurveyTypes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Base.Api.Models.Versions
{
    [Table("config.Versions")]
    public class Version : Base
    {
        public Version()
        {
        }

        [Required]
        [StringLength(256)]
        [Column("Version")]
        public string VersionName { get; set; }

        public string Description { get; set; }

        public bool IsPublished { get; set; }

        public string SurveyTypeId { get; set; }

        public virtual SurveyType SurveyType { get; set; }

        public DateTime? PublishDate { get; set; }

        public DateTime? ClosingDate { get; set; }

        public string ClosingMotive { get; set; }

        public string UserId { get; set; }

        public bool IsImpacted { get; set; }
        
    }
}