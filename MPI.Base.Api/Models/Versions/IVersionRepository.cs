﻿namespace MPI.Base.Api.Models.Versions
{
    public interface IVersionRepository : IRepository<Version>
    {
    }
}
