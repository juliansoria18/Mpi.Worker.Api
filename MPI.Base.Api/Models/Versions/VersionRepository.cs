﻿namespace MPI.Base.Api.Models.Versions
{
    public class VersionRepository : Repository<Version>, IVersionRepository
    {
        public VersionRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}