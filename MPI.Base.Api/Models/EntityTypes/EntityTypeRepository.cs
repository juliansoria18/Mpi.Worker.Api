﻿namespace MPI.Base.Api.Models.EntityTypes
{
    public class EntityTypeRepository : Repository<EntityType>, IEntityTypeRepository
    {
        public EntityTypeRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}