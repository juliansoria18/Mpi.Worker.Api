﻿namespace MPI.Base.Api.Models.EntityTypes
{
    public interface IEntityTypeRepository : IRepository<EntityType>
    {
    }
}
