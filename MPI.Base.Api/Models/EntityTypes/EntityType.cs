﻿using MPI.Base.Api.Models.EntityFields;
using MPI.Base.Api.Models.Organizations;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Base.Api.Models.EntityTypes
{
    [Table("entities.EntityType")]
    public class EntityType : Base
    {
        public EntityType()
        {
            EntityFields = new HashSet<EntityField>();
        }

        [Required]
        [StringLength(128)]
        [Column("EntityType")]
        public string EntityTypeName { get; set; }

        [StringLength(1024)]
        public string Icon { get; set; }

        public string OrganizationId { get; set; }

        public virtual Organization Organization { get; set; }

        [JsonIgnore]
        public virtual ICollection<EntityField> EntityFields { get; set; }

    }
}