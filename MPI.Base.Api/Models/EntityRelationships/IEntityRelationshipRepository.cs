﻿namespace MPI.Base.Api.Models.EntityRelationships
{
    public interface IEntityRelationshipRepository : IRepository<EntityRelationship>
    {
    }
}
