﻿namespace MPI.Base.Api.Models.EntityRelationships
{
    public class EntityRelationshipRepository : Repository<EntityRelationship>, IEntityRelationshipRepository
    {
        public EntityRelationshipRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}