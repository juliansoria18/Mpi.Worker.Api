﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MPI.Base.Api.Models.EntityRelationshipTypes;
using MPI.Base.Api.Models.EntityTypes;
using MPI.Base.Api.Models.Organizations;
using Newtonsoft.Json;

namespace MPI.Base.Api.Models.EntityRelationships
{
    [Table("config.EntityRelationships")]
    public class EntityRelationship : Base
    {
        [Required]
        public string OrganizationId { get; set; }

        [JsonIgnore]
        public virtual Organization Organization { get; set; }

        [ForeignKey("LeftEntityRelationship")]
        public string LeftEntityRelationshipId { get; set; }

        [JsonIgnore]
        public virtual EntityType LeftEntityRelationship { get; set; }

        [ForeignKey("RightEntityRelationship")]
        public string RightEntityRelationshipId { get; set; }

        [JsonIgnore]
        public virtual EntityType RightEntityRelationship { get; set; }

        [Required]
        public string EntityRelationshipTypeId { get; set; }

        [JsonIgnore]
        public virtual EntityRelationshipType EntityRelationshipType { get; set; }
    }
}